class Api::AuthenticationController < ApplicationController
  before_action :authenticate_request, except: :login

  def login
    user = User.find_by_email(params[:user][:email])
    if user&.authenticate(params[:user][:password])
      token = JsonWebToken.encode(user_id: user.id)
      render json: { token: token,
                     first_name: user.name.split(' ')[0],
                     admin: user.admin }, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  def logout
    render json: { message: 'Logged out' }, status: :ok
  end
end
