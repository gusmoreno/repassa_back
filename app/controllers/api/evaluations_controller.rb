class Api::EvaluationsController < ApplicationController
  before_action :authenticate_request

  # GET /evaluations
  def index
    received_evaluations = current_user.received_evaluations
    applied_evaluations = current_user.applied_evaluations
    render json: { received: ActiveModelSerializers::SerializableResource.new(received_evaluations, each_serializer: EvaluationSerializer),
                   applied: ActiveModelSerializers::SerializableResource.new(applied_evaluations, each_serializer: EvaluationSerializer) }
  end

  # POST /evaluations
  def create
    evaluation = Evaluation.new(evaluation_params)
    evaluation.evaluator_id = current_user.id
    if evaluation.save
      render json: evaluation, status: :created
    else
      render json: evaluation.errors, status: :unprocessable_entity
    end
  end


  private

    # Only allow a trusted parameter "white list" through.
    def evaluation_params
      params.require(:evaluation).permit(:evaluator_id, :evaluee_id, :grade, :start_date, :end_date, :comments)
    end
end
