class Api::UsersController < ApplicationController
  before_action :authenticate_request
  before_action :set_user, only: [:edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all.order("name asc")
    render json: { users: ActiveModelSerializers::SerializableResource.new(@users, each_serializer: UserSerializer) }, status: :ok
  end

  # GET /users/:id
  def edit
    render json: @user, status: :ok
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # PATCH /users/:id
  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /users/:id
  def destroy
    if @user.destroy
      render json: { message: 'Removido' }, status: :ok
    else
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # GET /users/managed
  def managed
    users = current_user.users.select("name as label, id as value").order("name asc")
    render json: { users: users }, status: :ok
  end

  # GET /users/manager_options
  def manager_options
    users = User.where.not(id: params[:id]).select("name as label, id as value").order("name asc")
    render json: { users: users }, status: :ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :username, :email, :admin, :manager_id,
                                 :password, :password_confirmation)
  end
end
