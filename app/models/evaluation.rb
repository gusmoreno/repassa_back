class Evaluation < ApplicationRecord

  belongs_to :evaluator, foreign_key: :evaluator_id, class_name: 'User'
  belongs_to :evaluee, foreign_key: :evaluee_id, class_name: 'User'

  validates_presence_of :grade, :evaluator_id, :evaluee_id, :start_date, :end_date

  validate :evaluee_is_managed_user

  # validates if the evaluee is among the users managed by the evaluator
  def evaluee_is_managed_user
    if self.evaluator_id.blank? || self.evaluee_id.blank? || User.find(self.evaluator_id).users.where(id: self.evaluee_id).empty?
      self.errors.add(:evaluator_id, 'não pode avaliar este colaborador')
    end
  end
end
