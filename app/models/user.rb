class User < ApplicationRecord
  has_secure_password

  has_many :users, foreign_key: :manager_id, class_name: 'User'
  belongs_to :manager, foreign_key: :manager_id, class_name: 'User', optional: true

  has_many :applied_evaluations, foreign_key: :evaluator_id, class_name: 'Evaluation'
  has_many :received_evaluations, foreign_key: :evaluee_id, class_name: 'Evaluation'

  validates_presence_of :name, :email
  validates_uniqueness_of :email

  before_destroy :check_if_user_is_related_to_evaluations, :check_if_user_is_manager

  def check_if_user_is_related_to_evaluations
    unless self.applied_evaluations.empty? && self.received_evaluations.empty?
      self.errors.add(:base, 'Não pôde ser removido pois possui avaliações recebidas ou aplicadas')
      throw(:abort)
    end
  end

  def check_if_user_is_manager
    unless User.where(manager_id: self.id).empty?
      self.errors.add(:base, 'Não pôde ser removido pois gerencia outros usuários')
      throw(:abort)
    end
  end

end
