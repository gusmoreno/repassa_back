class EvaluationSerializer < ActiveModel::Serializer
  attributes :id, :evaluator_id, :evaluee_id, :grade, :comments,
             :start_date, :end_date

  belongs_to :evaluator, foreign_key: :evaluator_id, class_name: 'User'
  belongs_to :evaluee, foreign_key: :evaluee_id, class_name: 'User'
end
