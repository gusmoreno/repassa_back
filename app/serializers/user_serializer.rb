class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :admin, :manager_id, :manager_name

  def manager_name
    object.manager.blank? ? 'Não possui' : object.manager.name
  end
end
