Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: :json } do

    post 'login' => 'authentication#login'
    delete 'logout' => 'authentication#logout'

    resources :evaluations, only: [:index, :create]

    resources :users, except: [:new, :show] do
      collection do
        get :managed
      end
      member do
        get :manager_options
      end
    end
  end
end
