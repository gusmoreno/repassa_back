class CreateEvaluations < ActiveRecord::Migration[5.2]
  def change
    create_table :evaluations do |t|
      t.integer :grade
      t.date :start_date
      t.date :end_date
      t.string :comments
      t.references :evaluator, index: true, foreign_key: { to_table: :users }
      t.references :evaluee, index: true, foreign_key: { to_table: :users }
      t.timestamps
    end
  end
end
