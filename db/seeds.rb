# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: 'Helo', email: 'helo.teste@teste.com', password: '123456', password_confirmation: '123456', manager_id: nil, admin: true)

User.create(name: 'Gustavo', email: 'gustavo.teste@teste.com', password: '123456', password_confirmation: '123456', manager_id: 1, admin: true)

User.create(name: 'Lucas', email: 'lucas.teste@teste.com', password: '123456', password_confirmation: '123456', manager_id: 2, admin: false)
User.create(name: 'Luiz', email: 'luiz.teste@teste.com', password: '123456', password_confirmation: '123456', manager_id: 2, admin: false)
User.create(name: 'Luciana', email: 'luciana.teste@teste.com', password: '123456', password_confirmation: '123456', manager_id: 2, admin: false)

Evaluation.create(grade: 5, evaluator_id: 1, evaluee_id: 2, start_date: Date.today - 10.days,
                  end_date: Date.today, comments: 'Desempenho excelente, parabéns.')
Evaluation.create(grade: 5, evaluator_id: 2, evaluee_id: 5, start_date: Date.today - 10.days,
                  end_date: Date.today, comments: 'Gostamos muito de seu trabalho durante o período')
Evaluation.create(grade: 4, evaluator_id: 2, evaluee_id: 4, start_date: Date.today - 10.days,
                  end_date: Date.today, comments: 'Bom trabalho! Está no caminho certo.')
Evaluation.create(grade: 3, evaluator_id: 2, evaluee_id: 3, start_date: Date.today - 10.days,
                  end_date: Date.today, comments: 'Executou bem algumas coisas, porém é preciso melhorar em outras...')