require 'rails_helper'

RSpec.describe Api::EvaluationsController, type: :controller do

  describe 'EvaluationsController' do

    let!(:evaluator) { create(:user) }
    let!(:evaluee) { create(:user, email: 'email2@teste.com', manager_id: evaluator.id) }

    describe "GET #index" do

      let!(:evaluation) { create(:evaluation, evaluator_id: evaluator.id, evaluee_id: evaluee.id)}

      it 'returns array of received and applied evaluations' do
        subject.class.skip_before_action :authenticate_request
        allow(controller).to receive(:current_user).and_return(evaluator)
        get :index
        expect(JSON.parse(response.body)['applied'].count).to eq(1)
        expect(JSON.parse(response.body)['received'].count).to eq(0)
      end

    end

    describe "POST #create" do

      context "with valid attributes" do
        it "creates a new evaluation" do
          allow(controller).to receive(:current_user).and_return(evaluator)
          post :create, params: { evaluation: { evaluator_id: evaluator.id, evaluee_id: evaluee.id, grade: 4,
                                                start_date: Date.today - 10.days, end_date: Date.today } }
          expect(Evaluation.count).to eq(1)
        end

        it "responds with 201" do
          allow(controller).to receive(:current_user).and_return(evaluator)
          post :create, params: { evaluation: { evaluator_id: evaluator.id, evaluee_id: evaluee.id, grade: 4,
                                                start_date: Date.today - 10.days, end_date: Date.today } }
          expect(response.status).to eq(201)
        end
      end

      context "with invalid attributes" do
        it "does not save the new evaluation" do
          allow(controller).to receive(:current_user).and_return(evaluator)
          post :create, params: { evaluation: { evaluator_id: evaluator.id, evaluee_id: evaluee.id, grade: nil,
                                                start_date: Date.today - 10.days, end_date: Date.today } }
          expect(Evaluation.count).to eq(0)
        end

        it "responds with 422" do
          allow(controller).to receive(:current_user).and_return(evaluator)
          post :create, params: { evaluation: { evaluator_id: evaluator.id, evaluee_id: evaluee.id, grade: nil,
                                                start_date: Date.today - 10.days, end_date: Date.today } }
          expect(response.status).to eq(422)
        end
      end
    end
  end
end