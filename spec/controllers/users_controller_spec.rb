require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do

  describe 'UsersController' do

    describe "GET #index" do

      let!(:user) { create(:user) }

      it 'populates an array of users' do
        subject.class.skip_before_action :authenticate_request
        # allow(controller).to receive(:current_user).and_return(user)
        get :index
        expect(assigns(:users)).to eq([user])
      end

    end

    describe "POST #create" do

      context "with valid attributes" do
        it "creates a new user" do
          post :create, params: { user: { name: 'First Name', email: 'teste@email.com', admin: false,
                                  password: '123456', password_confirmation: '123456' } }
          expect(User.count).to eq(1)
        end

        it "responds with 201" do
          post :create, params: { user: { name: 'First Name', email: 'teste@email.com', admin: false,
                                          password: '123456', password_confirmation: '123456' } }
          expect(response.status).to eq(201)
        end
      end

      context "with invalid attributes" do
        it "does not save the new user" do
          post :create, params: { user: { name: 'First Name', email: nil } }
          expect(User.count).to eq(0)
        end

        it "responds with 422" do
          post :create, params: { user: { name: 'First Name', email: nil } }
          expect(response.status).to eq(422)
        end
      end
    end

    describe "PATCH #update" do

      before(:each) do
        @user = User.create(name: 'First Name', email: 'teste@email.com', admin: false,
                            password: '123456', password_confirmation: '123456')
      end

      context "with valid attributes" do
        it "updates the user" do
          patch :update, params: { id: @user.id, user: { name: 'First Name', email: 'teste@email.com', admin: true} }
          @user.reload
          expect(@user.admin).to eq(true)
        end

        it "responds with 200" do
          patch :update, params: { id: @user.id, user: { name: 'First Name', email: 'teste@email.com', admin: true} }
          expect(response.status).to eq(200)
        end
      end

      context "with invalid attributes" do
        it "does not update user" do
          patch :update, params: { id: @user.id, user: { name: 'First Name', email: nil, admin: true} }
          @user.reload
          expect(@user.admin).to eq(false)
        end

        it "responds with 422" do
          patch :update, params: { id: @user.id, user: { name: 'First Name', email: nil, admin: true} }
          @user.reload
          expect(response.status).to eq(422)
        end
      end
    end

    describe "DELETE #destroy" do

      before(:each) do
        @user = User.create(name: 'First Name', email: 'teste@email.com', admin: true,
                            password: '123456', password_confirmation: '123456')
      end

      context "cannot delete user" do
        it "does not find user with id" do
          expect { delete :destroy, params: { id: -1 } }.to raise_exception ActiveRecord::RecordNotFound
        end

        it "user manages another user" do
          user = User.create(name: 'Second Name', email: 'second@email.com', admin: false,
                             password: '123456', password_confirmation: '123456', manager_id: @user.id)
          delete :destroy, params: { id: @user.id }

          expect(JSON.parse(response.body)["errors"][0]).to eq('Não pôde ser removido pois gerencia outros usuários')
        end

        it "user has received or applied evaluation" do
          user = User.create(name: 'Second Name', email: 'second@email.com', admin: false,
                             password: '123456', password_confirmation: '123456', manager_id: @user.id)
          Evaluation.create(evaluator_id: @user.id, evaluee_id: user.id, grade: 5,
                            start_date: Date.today()-10.days, end_date: Date.today())
          delete :destroy, params: { id: @user.id }
          expect(JSON.parse(response.body)["errors"][0]).to eq('Não pôde ser removido pois possui avaliações recebidas ou aplicadas')
        end
      end

      context "can delete user" do
        it "removes user from DB" do
          delete :destroy, params: { id: @user.id }
          expect(User.all).not_to include @user
          expect { @user.reload }.to raise_exception ActiveRecord::RecordNotFound
        end

        it "responds with 200" do
          delete :destroy, params: { id: @user.id }
          expect(response.status).to eq(200)
        end
      end
    end
  end
end