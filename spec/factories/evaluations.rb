FactoryBot.define do
  factory :evaluation do
    evaluator_id nil
    evaluee_id nil
    grade 5
    start_date Date.today - 10.days
    end_date Date.today
  end
end
