FactoryBot.define do
  factory :user do
    name 'First User'
    email 'firstuser@email.com'
    admin true
    password '123456'
    password_confirmation '123456'
  end
end
