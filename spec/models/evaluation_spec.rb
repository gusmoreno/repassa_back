require 'rails_helper'

RSpec.describe Evaluation, type: :model do

  before(:each) do
    @evaluator = create(:user)
    @evaluee = create(:user, email: 'email2@teste.com', manager_id: @evaluator.id)
  end

  it "has a valid factory" do
    expect(create(:evaluation, evaluator_id: @evaluator.id, evaluee_id: @evaluee.id)).to be_valid
  end

  #campos obrigatórios
  it "is invalid without evaluator" do
    expect(build(:evaluation, evaluator_id: nil)).not_to be_valid
  end

  it "is invalid without evaluee" do
    expect(build(:evaluation, evaluee_id: nil)).not_to be_valid
  end

  it "is invalid without grade" do
    expect(build(:evaluation, evaluator_id: @evaluator.id, evaluee_id: @evaluee.id, grade: nil)).not_to be_valid
  end

  it "is invalid without start_date" do
    expect(build(:evaluation, evaluator_id: @evaluator.id, evaluee_id: @evaluee.id, start_date: nil)).not_to be_valid
  end

  it "is invalid without end_date" do
    expect(build(:evaluation, evaluator_id: @evaluator.id, evaluee_id: @evaluee.id, end_date: nil)).not_to be_valid
  end

  #business rules
  it "is invalid if evaluator does not manage the evaluee" do
    evaluation = build(:evaluation, evaluator_id: @evaluee.id, evaluee_id: @evaluator.id)
    evaluation.valid?
    expect( evaluation.errors[:evaluator_id] ).to include("não pode avaliar este colaborador")
  end
end
