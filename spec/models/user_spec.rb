require 'rails_helper'

RSpec.describe User, type: :model do

  it "has a valid factory" do
    expect(create(:user)).to be_valid
  end

  #campos obrigatórios
  it "is invalid without Nome" do
    expect(build(:user, name: nil)).not_to be_valid
  end

  it "is invalid without E-mail" do
    expect(build(:user, email: nil)).not_to be_valid
  end

  it "is invalid without Password" do
    expect(build(:user, password: nil)).not_to be_valid
  end

  #campos únicos
  it "is invalid if E-mail already exists" do
    create(:user, email: 'email@example.com')
    user2 = build(:user, email: 'email@example.com')
    user2.valid?
    expect( user2.errors[:email] ).to include("já está em uso")
  end
end
